# generates doc
node ../node_modules/jsdoc-to-json-schema -i ../src/index.js -o ../schema/bpmn2json.schema.json
node ../node_modules/jsdoc/jsdoc.js ../src/ -d html
# https://jsdoc.app/
# https://github.com/john-doherty/jsdoc-to-json-schema