
var xmldoc = require('xmldoc');

/**
 * Get the current value of the tag.
 *
 * @schema.name Person
 * @schema.description This is an example Person object marked up with JSON schema tags to allow schema generation
 * @param {string} firstParameter First parameter to add.  
 * @default firstParameter deerhunter
 * @param {string} secondParameter Second Parameter to add.  
 * @param {date} therdParameter Date Parameter to add.  
 * @returns {bpmn-json}
 * @example
 * gparseBPMN2Object("5", 15);
 * @schema.returned XX
 * @schema.type array
 * @schema.minItems 3
 * @schema.maxItems 6
 * @schema.required true
 * @schema.properties.bpm_xml string
 */
function parseBPMN2Object(bpm_xml){
    // parse the document using xmldoc
    var bpm_doc = new xmldoc.XmlDocument(bpm_xml);
    // console.info(bpm_doc.children) asdf
    var dags_dct = {"dag": []}
    for (definition_index in bpm_doc.children){
        var definition_item = bpm_doc.children[definition_index]
        if (definition_item.name == "bpmn2:process"){
            dag = {}
            if (definition_item.attr && definition_item.attr.id){
                dag["id"] = definition_item.attr.id
                dag["configuration"] = {}
                if (definition_item.attr.isExecutable){
                    
                }
                // TODO: output 
                dag["run"] = "definition_item.attr.id"
            }        
            // lets extract
            for (process_index in definition_item.children){
                // console.info(definition_item.children[process_index])
                var process_item = definition_item.children[process_index]
                // console.info("process_item ", process_item.name)
                if (process_item.attr && process_item.attr.id){

                    // lets get the type
                    var type = process_item.name.replace("bpmn2:", "")
                    if (!dag[type]){ dag[type] = []; }
                    // extract common info
                    var dag_process = {
                        "id": process_item.attr.id || "",
                        "text": process_item.attr.name || "" // is the text in the box
                    }
                    
                    // extract specific info
                    if (process_item.name == "bpmn2:sequenceFlow"){
                        dag_process["source"] = process_item.attr.sourceRef
                        dag_process["target"] = process_item.attr.targetRef
                    }

                    // extract specific info
                    if (process_item.name == "bpmn2:boundaryEvent"){
                        dag_process["source"] = process_item.attr.attachedToRef
                    }                    

                    dag[type].push(dag_process)

                }
            }
            dags_dct["dag"].push(dag)
        }
    }
    return dags_dct;
}

exports.parseBPMN2Object = parseBPMN2Object