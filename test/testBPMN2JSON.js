var expect = require('chai').expect;
var bpmn2json = require('../src/index');

var fs = require('fs'),
    path = require('path');

bpm_xml = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:bioc="http://bpmn.io/schema/bpmn/biocolor/1.0" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
  <bpmn2:process id="Process_1" isExecutable="false">
    <bpmn2:startEvent id="StartEvent_1">
      <bpmn2:outgoing>Flow_00hjg6q</bpmn2:outgoing>
    </bpmn2:startEvent>
    <bpmn2:task id="Activity_1bwm7xv" name="A">
      <bpmn2:incoming>Flow_00hjg6q</bpmn2:incoming>
      <bpmn2:outgoing>Flow_0zuw2o2</bpmn2:outgoing>
    </bpmn2:task>
    <bpmn2:sequenceFlow id="Flow_00hjg6q" sourceRef="StartEvent_1" targetRef="Activity_1bwm7xv" />
    <bpmn2:sequenceFlow id="Flow_0zuw2o2" sourceRef="Activity_1bwm7xv" targetRef="Activity_0hjluzs" />
    <bpmn2:exclusiveGateway id="Gateway_0sutxsl">
      <bpmn2:incoming>Flow_0jp8t9g</bpmn2:incoming>
      <bpmn2:outgoing>Flow_0yrpj0k</bpmn2:outgoing>
      <bpmn2:outgoing>Flow_0g807fy</bpmn2:outgoing>
    </bpmn2:exclusiveGateway>
    <bpmn2:sequenceFlow id="Flow_0jp8t9g" sourceRef="Activity_0hjluzs" targetRef="Gateway_0sutxsl" />
    <bpmn2:sequenceFlow id="Flow_0yrpj0k" sourceRef="Gateway_0sutxsl" targetRef="Activity_0io3dh4" />
    <bpmn2:task id="Activity_0foquu9" name="C">
      <bpmn2:incoming>Flow_0g807fy</bpmn2:incoming>
      <bpmn2:property id="Property_18iy8dg" name="__targetRef_placeholder" />
      <bpmn2:dataInputAssociation id="DataInputAssociation_0ah8b8j">
        <bpmn2:sourceRef>DataStoreReference_01ceytq</bpmn2:sourceRef>
        <bpmn2:targetRef>Property_18iy8dg</bpmn2:targetRef>
      </bpmn2:dataInputAssociation>
      <bpmn2:dataOutputAssociation id="DataOutputAssociation_1o9ijje">
        <bpmn2:targetRef>DataStoreReference_1g328jv</bpmn2:targetRef>
      </bpmn2:dataOutputAssociation>
    </bpmn2:task>
    <bpmn2:sequenceFlow id="Flow_0g807fy" sourceRef="Gateway_0sutxsl" targetRef="Activity_0foquu9" />
    <bpmn2:userTask id="Activity_0hjluzs" name="B">
      <bpmn2:incoming>Flow_0zuw2o2</bpmn2:incoming>
      <bpmn2:incoming>Flow_1jzr7in</bpmn2:incoming>
      <bpmn2:outgoing>Flow_0jp8t9g</bpmn2:outgoing>
    </bpmn2:userTask>
    <bpmn2:dataStoreReference id="DataStoreReference_1g328jv" name="BigQuery" />
    <bpmn2:dataStoreReference id="DataStoreReference_01ceytq" name="PSQL" />
    <bpmn2:sequenceFlow id="Flow_1jzr7in" name="Starts if winds are large" sourceRef="Event_1rwcmz0" targetRef="Activity_0hjluzs" />
    <bpmn2:startEvent id="Event_1rwcmz0">
      <bpmn2:outgoing>Flow_1jzr7in</bpmn2:outgoing>
      <bpmn2:conditionalEventDefinition id="ConditionalEventDefinition_0q2x3x0">
        <bpmn2:condition xsi:type="bpmn2:tFormalExpression" />
      </bpmn2:conditionalEventDefinition>
    </bpmn2:startEvent>
    <bpmn2:scriptTask id="Activity_0io3dh4" name="D">
      <bpmn2:incoming>Flow_0yrpj0k</bpmn2:incoming>
      <bpmn2:property id="Property_076hriw" name="__targetRef_placeholder" />
      <bpmn2:dataInputAssociation id="DataInputAssociation_0hsjn2v">
        <bpmn2:sourceRef>DataStoreReference_0kmd7ws</bpmn2:sourceRef>
        <bpmn2:targetRef>Property_076hriw</bpmn2:targetRef>
      </bpmn2:dataInputAssociation>
    </bpmn2:scriptTask>
    <bpmn2:dataStoreReference id="DataStoreReference_0kmd7ws" />
  </bpmn2:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNEdge id="Flow_1jzr7in_di" bpmnElement="Flow_1jzr7in">
        <di:waypoint x="448" y="30" />
        <di:waypoint x="710" y="30" />
        <di:waypoint x="710" y="218" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="535" y="-24" width="88" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0g807fy_di" bpmnElement="Flow_0g807fy" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <di:waypoint x="875" y="258" />
        <di:waypoint x="940" y="258" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0yrpj0k_di" bpmnElement="Flow_0yrpj0k" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <di:waypoint x="850" y="283" />
        <di:waypoint x="850" y="330" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0jp8t9g_di" bpmnElement="Flow_0jp8t9g" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <di:waypoint x="760" y="258" />
        <di:waypoint x="825" y="258" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0zuw2o2_di" bpmnElement="Flow_0zuw2o2" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <di:waypoint x="600" y="258" />
        <di:waypoint x="660" y="258" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_00hjg6q_di" bpmnElement="Flow_00hjg6q" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <di:waypoint x="448" y="258" />
        <di:waypoint x="500" y="258" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <dc:Bounds x="412" y="240" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1bwm7xv_di" bpmnElement="Activity_1bwm7xv" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <dc:Bounds x="500" y="218" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0sutxsl_di" bpmnElement="Gateway_0sutxsl" isMarkerVisible="true" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <dc:Bounds x="825" y="233" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0foquu9_di" bpmnElement="Activity_0foquu9" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <dc:Bounds x="940" y="218" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0gmdtqb_di" bpmnElement="Activity_0hjluzs" bioc:stroke="black" bioc:fill="rgba(255, 255, 255, 1)">
        <dc:Bounds x="660" y="218" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataStoreReference_1g328jv_di" bpmnElement="DataStoreReference_1g328jv">
        <dc:Bounds x="1015" y="85" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1017" y="55" width="46" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataStoreReference_01ceytq_di" bpmnElement="DataStoreReference_01ceytq">
        <dc:Bounds x="905" y="85" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="915" y="55" width="30" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_08xtixl_di" bpmnElement="Event_1rwcmz0">
        <dc:Bounds x="412" y="12" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="DataStoreReference_0kmd7ws_di" bpmnElement="DataStoreReference_0kmd7ws">
        <dc:Bounds x="685" y="355" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_189l461_di" bpmnElement="Activity_0io3dh4">
        <dc:Bounds x="800" y="330" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="DataInputAssociation_0ah8b8j_di" bpmnElement="DataInputAssociation_0ah8b8j">
        <di:waypoint x="939" y="135" />
        <di:waypoint x="967" y="218" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="DataOutputAssociation_1o9ijje_di" bpmnElement="DataOutputAssociation_1o9ijje">
        <di:waypoint x="1005" y="218" />
        <di:waypoint x="1035" y="135" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="DataInputAssociation_0hsjn2v_di" bpmnElement="DataInputAssociation_0hsjn2v">
        <di:waypoint x="735" y="378" />
        <di:waypoint x="800" y="374" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn2:definitions>
`


describe('parseBPMN2Object()', function () {
  it('should parse BPMN with datasources', function () {
    var bpmn_file = fs.readFileSync("./test/tap-fasteignir.bpmn");
    var bpmn_json = bpmn2json.parseBPMN2Object(bpmn_file.toString());
    // check root
    expect(Object.keys(bpmn_json)).eql(["dag"])
    // check for dag keys
    expect(Object.keys(bpmn_json["dag"]).length).eql(1)
    expect(Object.keys(bpmn_json["dag"][0])).eql(["id", "configuration", "run", "sequenceFlow", "task", "dataStoreReference", "endEvent", "startEvent"])

    // check for dag
    var dag = bpmn_json["dag"][0]
    expect(dag.id).eql("Process_1")

    // sequenceFlow
    expect(dag.sequenceFlow.length).to.equal(2)
    sequenceFlow = dag.sequenceFlow[0];
    // console.info(sequenceFlow)
    expect(Object.keys(sequenceFlow)).eql(["id", "text", "source", "target"])
    
    // task
    expect(dag.task.length).to.equal(1)
    task = dag.task[0];
    // console.info(task)
    expect(Object.keys(task)).eql(["id", "text"])
    // TODO: should have exec
    
    // dataStoreReference
    expect(dag.dataStoreReference.length).to.equal(2)
    dataStoreReference = dag.dataStoreReference[0];
    // console.info(dataStoreReference)
    expect(Object.keys(dataStoreReference)).eql(["id", "text"])
    // TODO: should have pull, push, exec

    // startEvent
    expect(dag.startEvent.length).to.equal(1)
    startEvent = dag.startEvent[0];
    // console.info(startEvent)
    expect(Object.keys(startEvent)).eql(["id", "text"])    

    // endEvent
    expect(dag.endEvent.length).to.equal(1)
    endEvent = dag.endEvent[0];
    // console.info(endEvent)
    expect(Object.keys(endEvent)).eql(["id", "text"])        
  });



  it('should parse BPMN with basic events', function () {
    var bpmn_file = fs.readFileSync("./test/events-basic.bpmn");  
    var bpmn_json = bpmn2json.parseBPMN2Object(bpmn_file.toString());
    console.info(JSON.stringify(bpmn_json));
  });

});